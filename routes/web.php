<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', function () {
    return view("login");
});
Route::get('/productlist', function () {
    return view("productlist");
});
Route::get('/viewproduct', function () {
    return view('viewproduct');
});
Route::get('/addproduct', function () {
    return view('addproduct');
});
Route::get('/myaccount', function () {
    return view('myaccount');
});
Route::get('/editproduct', function () {
    return view('editproduct');
});
Route::get('/delproduct', function () {
    return view('delproduct');
});

Route::get('/deluser', function () {
    return view('deluser');
});

Route::get('/addproduct','AddproductController@add');
Route::get('/viewproduct','ViewproductController@index');
Route::get('/myaccount','MyaccountController@index');
Route::get('/adduser','AdduserController@add');
Route::get('/product','DeleteController@deleteproduct');
//Route::get('/editpro','EditController@add');
Route::get('/user','UserdeleteController@ delete');





