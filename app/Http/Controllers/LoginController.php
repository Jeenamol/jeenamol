<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
use DB;
use Crypt;

class LoginController extends Controller
{
    public function login(Request $req){   

        $email = $req->email;
        $password = $req->password;
       
        $Userdetails = DB::table('users')->select('users.id','users.password')
                        ->where('users.email' , '=' , $req->email )
                        ->get();
        $pass = Crypt::decrypt($Userdetails[0]->password);
        if($Userdetails){
          if($password == $pass){
         
            $_SESSION['user'] =  $email;
           

            if( $email == 'email' )
                return redirect('/productlist');
            
          }
        }
        else{
            return redirect('/');
        }
    }

    public function logout() {
        logoutFn();
    }
}
